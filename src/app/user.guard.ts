import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './main/user.service';

@Injectable()
export class UserGuard implements CanActivate {

constructor(private userService: UserService) {
  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      // console.log('User-Guard: ' + this.userService.isLoggedIn());
       return true; // development
      // return this.userService.isLoggedIn();
  }
}
