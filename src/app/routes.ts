import { Routes } from '@angular/router';
import { LoginComponent } from './main/content/login/login.component';

import { AuthPortalGuard } from './auth-portal.guard';
import { UserGuard } from './user.guard';

import { FuseHomeComponent } from './main/content/home/home.component';
import { FusePublicComponent } from './main/content/public/public.component';
import { FuseAdminComponent } from './main/content/admin/admin.component';
import { FuseDevelopComponent } from './main/content/develop/develop.component';
import { FuseSalesComponent } from './main/content/sales/sales.component';
import { FuseCalendarComponent } from './main/content/calendar/calendar.component';
import { FuseMailComponent } from './main/content/mail/mail.component';
import { FuseProductionComponent } from './main/content/production/production.component';
import { ApplyForMemberComponent } from './main/content/home/membership-text/apply-for-member/apply-for-member.component';
import { FuseMitchComponent } from './main/content/mitch/mitch.component';
import { ApplyMemberComponent } from './main/content/apply-member/apply-member.component';



export const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: FuseHomeComponent,
    canActivate: [AuthPortalGuard]
  },
  {
     path: 'public',
     component: FusePublicComponent,
     canActivate: [AuthPortalGuard, UserGuard]
  },
    {
     path: 'production',
     component: FuseProductionComponent,
     canActivate: [AuthPortalGuard, UserGuard]
  },
  {
     path     : 'admin', 
     component: FuseAdminComponent,
     canActivate: [AuthPortalGuard, UserGuard]
  },
  {
     path     : 'develop', 
     component: FuseDevelopComponent,
     canActivate: [AuthPortalGuard, UserGuard]
  },
  {
     path     : 'sales', 
     component: FuseSalesComponent,
     canActivate: [AuthPortalGuard, UserGuard]
        
  },
  {
    path: 'apps/mail',
    component: FuseMailComponent,
    canActivate: [AuthPortalGuard, UserGuard]

  },
  {
    path: 'apps/calendar',
    component: FuseCalendarComponent,
    canActivate: [AuthPortalGuard, UserGuard]

  },
  {
     path     : 'mitch', 
     component: FuseMitchComponent,
     canActivate: [AuthPortalGuard, UserGuard]
        
  },
  {
    path: 'apply-member',
    component: ApplyMemberComponent,
    canActivate: [AuthPortalGuard, UserGuard]

  },
  {
    path: '**',
    component: LoginComponent,
  },  
];


