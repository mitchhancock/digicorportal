import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

import 'hammerjs';
import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
import { FuseMainModule } from './main/main.module';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { FuseConfigService } from './core/services/config.service';
import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { FuseSalesModule } from './main/content/sales/sales.module';
import { FuseMailModule } from './main/content/mail/mail.module';
import { FuseCalendarModule } from './main/content/calendar/calendar.module';
import { FuseMitchModule } from './main/content/mitch/mitch.module';
import { FuseHomeModule } from './main/content/home/home.module';
import { FuseDevelopModule } from './main/content/develop/develop.module';
import { FuseProductionModule } from './main/content/production/production.module';
import { FuseAdminModule } from './main/content/admin/admin.module';
import { FusePublicModule } from './main/content/public/public.module';
import { ApplyMemberModule } from './main/content/apply-member/apply-member.module';

import { FakeDbService } from './dataSources/fake-db.service';
import { LoginModule } from './main/content/login/login.module';
import { routes } from './routes';
import { AuthPortalGuard } from './auth-portal.guard';
import { PortaluserService } from './main/portaluser.service';
import { UserService } from './main/user.service';
import { UserGuard } from './user.guard';


const appRoutes: Routes = routes;

@NgModule({
    declarations: [
      AppComponent
    ],
    imports     : [
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),
        HttpClientModule,
        
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        
        SharedModule,
        FuseMainModule,
        FuseSalesModule,
        FuseMailModule,
        FuseMitchModule,
        FuseHomeModule,
        FuseCalendarModule,
        FuseDevelopModule,
        FuseProductionModule,
        FuseAdminModule,
        FusePublicModule,
        FormsModule,
        ApplyMemberModule,
        LoginModule                
    ],
    exports: [RouterModule],
    providers   : [
        FuseSplashScreenService,
        FuseConfigService,
        FuseNavigationService,
        AuthPortalGuard,
        PortaluserService,
        UserService,        
        UserGuard
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
