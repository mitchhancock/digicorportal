import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { FuseConfigService } from '../../core/services/config.service';
import { UserService } from './../user.service'; 

@Component({
    selector: 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    providers: []
})

export class FuseToolbarComponent {
    _subscription: any;
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    horizontalNav: boolean;
    userName: string = 'Guest';

    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private userService: UserService,
    ) {
        this.userName = userService.actualUser.name;
        
          this._subscription = userService.userChange.subscribe((value) => {
             this.userName = value;
          });
      
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon': 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon': 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon': 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                'id': 'uk',
                'title': 'English',
                'flag': 'uk'
            },
            {
                'id': 'ge',
                'title': 'German',
                'flag': 'ge'

            },
            {
                'id': 'es',
                'title': 'Spanish',
                'flag': 'es'
            },
            {
                'id': 'fr',
                'title': 'French',
                'flag': 'fr'
            }
        ];

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if (event instanceof NavigationStart) {
                    this.showLoadingBar = true;
                }
                if (event instanceof NavigationEnd) {
                    this.showLoadingBar = false;
                }
            });

        this.fuseConfig.onSettingsChanged.subscribe((settings) => {
            this.horizontalNav = settings.layout.navigation === 'top';
        });

    }

  getLoginStatus(): boolean {
     return this.userService.isLoggedIn();
  }
  
  getAvatar(): string {
    return this.userService.actualUser.avatar;
  }
   logout(): void {
   // -- logout
   this.userService.logout();
   this.router.navigate(['/home']);
  }

    search(value) {
        // Do your search here...
        console.log(value);
    }

    ngOnDestroy() {
        // prevent memory leak when component destroyed
         this._subscription.unsubscribe();
    }

}
