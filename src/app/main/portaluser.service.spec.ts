import { TestBed, inject } from '@angular/core/testing';

import { PortaluserService } from './portaluser.service';

describe('PortaluserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PortaluserService]
    });
  });

  it('should be created', inject([PortaluserService], (service: PortaluserService) => {
    expect(service).toBeTruthy();
  }));
});
