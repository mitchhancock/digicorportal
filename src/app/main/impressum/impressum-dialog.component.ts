import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
    selector   : 'impressum-dialog',
    templateUrl: 'impressum-dialog.html',
    styleUrls  : ['impressum-dialog.scss']
    
})

export class ImpressumDialogComponent
{
    constructor(public dialogRef: MatDialogRef<ImpressumDialogComponent>)
    {
    }
  
  
public closeDialog(): void
   {
    this.dialogRef.close();
   }
  
}

