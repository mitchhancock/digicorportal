import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FuseNavigationService } from './../core/components/navigation/navigation.service';
import { NavigationModelLogout } from './../navigation-custom.model';


 const url_user = 'api/user';

interface User {
    'name': string;
    'lastName': string;
    'company': string;
    'password': string;
    'username': string;
    'avatar': string;
    'calendarUsername': string;
  }

@Injectable()
export class UserService {

  users: any; 
  actualUser: User = {
    'name': 'Guest',
    'lastName': 'Guest',
    'company': 'none',
    'password': 'guest',
    'username': 'guest',
    'avatar': 'assets/images/avatars/profile.jpg',
    'calendarUsername' : 'mitchs.email999@gmail.com'
  };
  navModelLogout: NavigationModelLogout;
  
  userChange: Subject<string> = new Subject<string>();
  loggedIn: boolean;
  
  constructor(private http: HttpClient,
              private navbarService: FuseNavigationService) { 
   
   this.navModelLogout = new NavigationModelLogout();
   
   // User DB 
   //this.http.get(url_user).subscribe(res => {
   //   this.users = res;
   // });
   this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/login?username=' + 'mitch' + '&password=' + 'mitch').subscribe(res => {
     this.users = res;
     console.log(res);
    });
   
    
   // login status init
   this.loggedIn = false;
  }
  
  isLoggedIn(): boolean {
   return this.loggedIn;
  }
  
  login(username: string, password: string): boolean {

   // search user database
        let user = {
          'avatar': 'assets/images/avatars/arnd.jpg',
          'company': 'Airbus',
          'email': 'arnd.schirrmann@airbus.com',
          'id': 1,
          'lastName': 'schirrmann',
          'name': 'arnd',
          'password': 'arnd',
          'username': 'arnd',
          'calendarUsername': 'mitchs.email999@gmail.com'
        };
        console.log(user);
        this.loggedIn = true;
        this.actualUser = user;
        //console.log(this.actualUser);
        this.userChange.next(this.actualUser.name);
        //console.log('New User: ' + this.actualUser);
        return true;
  }

  loginT(user: Object): boolean {
    if (user != false) {
      this.loggedIn = true;
      this.actualUser = <User>user;
      this.actualUser.avatar = 'http://icemain.hopto.org:8053/portalbackend/getImage.jsp?imageId=' + this.actualUser.avatar;
      this.actualUser.calendarUsername = 'mitchs.email999@gmail.com';
      this.userChange.next(this.actualUser.name);
      return true;
    }
    return false;
  }


  logout(): void {
   this.loggedIn = false; 
   this.actualUser = {  // Guest user 
                        'name': 'Guest',
                        'lastName': 'Guest',
                        'company': 'none',
                        'password': 'guest',
                        'username': 'guest',
                        'avatar': 'assets/images/avatars/profile.jpg',
                        'calendarUsername': 'mitchs.email999@gmail.com'
                       }; 
   this.userChange.next(this.actualUser.name);
    
   // -- update Navbar
   this.navbarService.setNavigationModel( this.navModelLogout); 
  } 
  
}
