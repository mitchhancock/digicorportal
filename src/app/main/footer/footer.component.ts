import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ImpressumDialogComponent } from './../impressum/impressum-dialog.component';

@Component({
    selector   : 'fuse-footer',
    templateUrl: './footer.component.html',
    styleUrls  : ['./footer.component.scss']
})
export class FuseFooterComponent implements OnInit
{
   
  
     constructor(public dialog: MatDialog)
     {
     }

    ngOnInit()
    {
    }

   openDialog()
    {
     let dialogRef = this.dialog.open(ImpressumDialogComponent);
    }

}


