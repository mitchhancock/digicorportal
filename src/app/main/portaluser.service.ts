import { Injectable } from '@angular/core';

@Injectable()
export class PortaluserService {

  loggedIn: boolean;
  
  constructor() { 
   this.loggedIn = false;
  
  }
  
  isLoggedIn(): boolean {
   return this.loggedIn;
  }
  
  loginUser(): void {
   this.loggedIn = true; 
  }

}
