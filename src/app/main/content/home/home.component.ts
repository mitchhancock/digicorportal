import { Component, OnInit } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from './../../user.service'; 


@Component({
  selector: 'fuse-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: []
})
export class FuseHomeComponent implements OnInit  {
  
  constructor(private userService: UserService){ 
  
  }
      
  getLoginStatus(): boolean {
   return this.userService.isLoggedIn();
  }
  
      
  ngOnInit(){ 
  
  }

}



