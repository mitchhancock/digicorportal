import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'message-sent-dlg',
  templateUrl: './message-sent-dlg.component.html',
  styleUrls: ['./message-sent-dlg.component.scss']
})
export class MessageSentDlgComponent {

  name: string = 'none';
  email: string = 'none';
  phone: string = 'none';
  message: string = 'none';
    
 constructor(public dialogRef: MatDialogRef<MessageSentDlgComponent>, @Inject(MAT_DIALOG_DATA) public data: any)  {
    this.name = data.name;
    this.email = data.email;
    this.phone = data.phone;   
    this.message = data.message;   
  }
    
 public closeDialog(): void
   {
    this.dialogRef.close();
   }
  
}
