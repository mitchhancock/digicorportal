import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageSentDlgComponent } from './message-sent-dlg.component';

describe('MessageSentDlgComponent', () => {
  let component: MessageSentDlgComponent;
  let fixture: ComponentFixture<MessageSentDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageSentDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageSentDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
