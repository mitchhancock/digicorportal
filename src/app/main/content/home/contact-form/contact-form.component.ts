import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GoogleMapsComponent } from './../google-maps/google-maps.component';

import { FormBuilder, FormGroup, NgForm, Validators, PatternValidator } from '@angular/forms';


import { MatDialog, MatDialogRef } from '@angular/material';
import { MessageSentDlgComponent } from './message-sent-dlg/message-sent-dlg.component';


@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
  providers: []
})

export class ContactFormComponent implements OnInit{

    name: string = '';
    email: string = '';
    phone: string = '';
    message: string = '';
    
    contactFormErrors: any;
    contactForm: FormGroup;
  
    constructor(
      public dialog: MatDialog,
      private formBuilder: FormBuilder)
    {
    
     this.contactFormErrors = {
          name : {},
          email : {},
          phone : {},
          message : {}
       };

    }

   ngOnInit()
    {
      this.contactForm = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', [Validators.required, Validators.pattern('[0-9]{1,125}')]],
        message: ['', Validators.required]
        });

        this.contactForm.valueChanges.subscribe(() => {
            this.onContactFormValuesChanged();
        });
    }

  onContactFormValuesChanged()
    {
        for ( const field in this.contactFormErrors )
        {
            if ( !this.contactFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.contactFormErrors[field] = {};

            // Get the control
            const control = this.contactForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.contactFormErrors[field] = control.errors;
            }
        }
  }
  

   openSentDialog()
    {
      // sent message 
       let dialogRef = this.dialog.open(MessageSentDlgComponent, {data: {name: this.name, email: this.email, phone: this.phone, message: this.message}});
      
      // reset form   
      this.name = '';
      this.email = '';
      this.phone = '';
      this.message = '';
     
      // Form reset
      this.contactForm.reset();
          
   }
}
