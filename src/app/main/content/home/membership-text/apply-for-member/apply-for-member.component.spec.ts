import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyForMemberComponent } from './apply-for-member.component';

describe('ApplyForMemberComponent', () => {
  let component: ApplyForMemberComponent;
  let fixture: ComponentFixture<ApplyForMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyForMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyForMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
