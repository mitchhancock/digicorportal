import { ApplyForMemberComponent } from './membership-text/apply-for-member/apply-for-member.component';
import { NgModule, ApplicationRef  } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { FuseHomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';

import { WelcomeTextComponent } from './welcome-text/welcome-text.component';
import { MembershipTextComponent } from './membership-text/membership-text.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { GoogleMapsComponent } from './google-maps/google-maps.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { MessageSentDlgComponent } from './contact-form/message-sent-dlg/message-sent-dlg.component';

const childRoutes = [
    {
        path: 'apply',   
        redirectTo: 'apply-member'
    }
];

@NgModule({ 
    declarations: [
        ContactFormComponent,
        GoogleMapsComponent,
        FuseHomeComponent,
        LoginFormComponent,
        RegisterFormComponent,
        WelcomeTextComponent,
        MembershipTextComponent ,
        ApplyForMemberComponent,
        MessageSentDlgComponent   
    ],
    imports     : [
        SharedModule,
        FuseWidgetModule,
        BrowserModule,
        RouterModule.forChild(childRoutes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCCbIepK2AE-GPYCocWW7WY2_HT9-MLM3g'
        }),
    ], 
    entryComponents: [
        MessageSentDlgComponent
    ],    
    exports     : [
        FuseHomeComponent
    ],
    providers : [ ]
})

export class FuseHomeModule
{
}
