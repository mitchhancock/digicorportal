import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router} from '@angular/router';

import { UserService } from './../../../user.service'; 
import { invalid } from 'moment';
import { FuseNavigationService } from './../../../../core/components/navigation/navigation.service';
import { NavigationModelLogin } from './../../../../navigation-custom.model';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: []
})
export class LoginFormComponent implements OnInit {
  _subscription: any;
  loginForm: FormGroup;
  loginFormErrors: any;
  loginResult: boolean;
  username: string;
  password: string;
  loggedUser: string = 'undefiniert';
  hide = true;
  naviModelLogin: NavigationModelLogin;
  
  constructor(
     private router: Router,
     private formBuilder: FormBuilder,
     private userService: UserService,
     private navbarService: FuseNavigationService,
     private http: HttpClient
) {
     this.naviModelLogin = new NavigationModelLogin();
  
     this.loginFormErrors = {
            username : {},
            password : {}
        };
      
      this.loginResult = true;
      this.username = '';
      this.password = '';
    
      this.loggedUser = this.userService.actualUser.name;
      
      this._subscription = userService.userChange.subscribe((value) => {
             this.loggedUser = value;
          });
 
    }
  
  getLoginStatus(): boolean {
   return this.userService.isLoggedIn();
  }
  
  getUser(): string {
   return this.userService.actualUser.name;
  }
  
  getAvatar(): string {
     return this.userService.actualUser.avatar;
  }
    
  logout(): void {
   // -- logout
   this.userService.logout();
   this.router.navigate(['/home']);
  }
  
  onLogin(username, password) {
    this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/login?username=' + username + '&password=' + password).subscribe(data => {
      if (this.userService.loginT(data)) {
          // -- login 
          this.loginResult = true;
          //this.router.navigate(['/public']);
          // -- update Navbar
          this.navbarService.setNavigationModel(this.naviModelLogin);
        }
        else {
          // -- wrong credentials
          this.loginResult = false;

        }
    });
  }

 ngOnInit()
    {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    onLoginFormValuesChanged()
    {
        for ( const field in this.loginFormErrors )
        {
            if ( !this.loginFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

   ngOnDestroy() {
        // prevent memory leak when component destroyed
         this._subscription.unsubscribe();
    }

}
