import { Component, ViewChild, OnInit } from '@angular/core';
import { AgmCoreModule, AgmMap } from '@agm/core';


@Component({
    selector   : 'app-google-maps',
    templateUrl: './google-maps.component.html',
    styleUrls  : ['./google-maps.component.scss'],
    
})

export class GoogleMapsComponent implements OnInit
{
    @ViewChild(AgmMap) agmMap: AgmMap;
   
    
    lat = 53.5365315;
    lng = 9.8660684;
    
    zoom: number = 15;
  
    constructor()
    {
    }
  
  ngOnInit(){
     // this.agmMap.triggerResize();
  }
  
}
