import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AeroNewsFakeDb } from '../../../../dataSources/aero-news';


@Component({
    selector   : 'app-platform-kpi',
    templateUrl: './platform-kpi.component.html',
    styleUrls  : ['./platform-kpi.component.scss']
})
export class PlatformKpiComponent implements OnInit
{
    news: any[];
    loadingIndicator = true;
    reorderable = true;

    constructor(private http: HttpClient)
    {

    }

    ngOnInit()
    {
        this.http.get('api/aeronews')
            .subscribe((aeronews: any) => {
                this.news = aeronews;
                this.loadingIndicator = false; 
            });
  }
              
}

