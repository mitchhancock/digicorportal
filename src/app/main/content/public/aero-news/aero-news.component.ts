import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AeroNewsFakeDb } from '../../../../dataSources/aero-news';


@Component({
    selector   : 'app-aero-news',
    templateUrl: './aero-news.component.html',
    styleUrls  : ['./aero-news.component.scss']
})
export class AeroNewsComponent implements OnInit
{
    news: any[];
    loadingIndicator = true;
    reorderable = true;

    constructor(private http: HttpClient)
    {

    }

    ngOnInit()
    {
        this.http.get('api/aeronews')
            .subscribe((aeronews: any) => {
                this.news = aeronews;
                this.loadingIndicator = false; 
            });
  }
              
}

