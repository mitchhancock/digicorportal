 export class MemberFilter{
 
 public static technologies = [
        'Mechanics',
        'Electronics',
        'ALM'
    ];
  
  public static experiences = [
        'Design & Built',
        'Built',
        'Design'
    ];
   
   public static regions = [
        'Europe',
        'Asia',
        'North America'
    ];
   
  public static aTA = [
        '21',
        '23',
        '24'
    ]; 
   public static certificates = [
        'Part 145',
        'AS 9100',
        'AQAP'
    ]; 
 }
