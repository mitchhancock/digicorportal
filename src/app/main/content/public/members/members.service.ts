import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const url_members = 'api/members';

@Injectable()
export class MembersService {

  constructor(private http: HttpClient) {}
  
  members: any;
  
  getMembers() {
    this.http.get(url_members).subscribe(res => {
      this.members = res;
    });
  }   
   
}
