import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MembersService } from './members.service';
import { MemberFilter } from './member-filter';

import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MembersComponent implements OnInit {

   members: any[];
   loadingIndicator = true;
   reorderable = true;
   selectedMember: any[];
   temp: any[];
   
   searchText: string = '';
  
   // Search autocomplete init
   searchControl: FormControl = new FormControl(); 
   technologies = MemberFilter.technologies;
   experiences = MemberFilter.experiences;
   regions = MemberFilter.regions;
   ataChapter = MemberFilter.aTA;
   certificates = MemberFilter.certificates;
   
  constructor(private dataService: MembersService) { }

  ngOnInit() {
  
    this.searchText = '';
   
    // get data from DB
    this.dataService.getMembers();
    this.loadingIndicator = false;
   
    // init datasets
    this.members = this.dataService.members; 
    this.temp = this.members;
   
    // select 1st row
    this.selectedMember = [this.members[0]];
      
  }

  
  onMemberSelect({ selected }) {
    // update selected row
    this.selectedMember = selected;
  }
  
  updateFilter(event) {
    const val = this.searchText.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return (d.companyName.toLowerCase().indexOf(val) !== -1 || 
              d.activities.toLowerCase().indexOf(val) !== -1 ||  
              d.certificates.toLowerCase().indexOf(val) !== -1 ||  
              d.region.toLowerCase().indexOf(val) !== -1 ||  
              d.type.toLowerCase().indexOf(val) !== -1) || !val;
     });
  
    // update the rows
    this.members = temp;
    
    // Select 1st
    if (this.members.length > 0){
       this.selectedMember = [this.members[0]];  
       }
  }
  
  onMembersActivate(event) {
    // console.log('Activate Event', event);
  }
  
}
