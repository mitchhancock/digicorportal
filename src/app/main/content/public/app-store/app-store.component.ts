import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AppStoreFakeDb } from '../../../../dataSources/app-store';
import { AppstoreService } from './appstore.service';
 
@Component({
  selector: 'app-app-store',
  templateUrl: './app-store.component.html',
  styleUrls: ['./app-store.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppStoreComponent implements OnInit {

   apps: any[];
   owned_apps: any[];
   loadingIndicator = true;
   reorderable = true;
   selectedApp: any[];
   temp: any[];
   canBuy: boolean = false;
   
   searchText: string = '';

  
  constructor(private dataService: AppstoreService) { }

  ngOnInit() {
   
   this.searchText = '';
   // get data from DB
   this.dataService.getApps();
   this.dataService.getOwnedApps();
   this.loadingIndicator = false;
   
   // init datasets
   this.apps = this.dataService.apps; 
   this.temp = this. apps;
   this.owned_apps = this.dataService.ownedapps; 
   // select 1st row
   this.selectedApp = [this.apps[0]];
     
  }

  onAppsSelect({ selected }) {
    // update selected row
    this.selectedApp = selected;
    this.canBuy = (this.findAppById(this.owned_apps, this.selectedApp[0].id) == null);
  }
  
  addApptoOwnedApps(){
    // push selected dataset to OwnedApps
    
    let newRow = {id: this.selectedApp[0].id, name: this.selectedApp[0].name, provider: this.selectedApp[0].provider, type: this.selectedApp[0].type, status: 'install...'};
    
    this.dataService.addOwnedApps(newRow);
        
    // reload data
    this.owned_apps = this.dataService.ownedapps; 
    this.canBuy = false;
  
  }
  
  updateFilter(event) {
    const val = this.searchText.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return (d.name.toLowerCase().indexOf(val) !== -1 ||
              d.provider.toLowerCase().indexOf(val) !== -1 ||
              d.type.toLowerCase().indexOf(val) !== -1 ||
              d.pay_model.toLowerCase().indexOf(val) !== -1) || !val;
     });
  
    // update the rows
    this.apps = temp;
    
    // Select 1st
    if (this.apps.length > 0){
       this.selectedApp = [this.apps[0]];  
       this.canBuy = (this.findAppById(this.owned_apps, this.selectedApp[0].id) == null);
    }
  }
  
   findAppById(appList: any[], id: number) {
       return appList.find(x => x.id === id);
     } 
  
  onAppsActivate(event) {
    // console.log('Activate Event', event);
  }
  
}
