import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const url_apps = 'api/apps';
const url_owned_apps = 'api/owned-apps';

@Injectable()
export class AppstoreService {
 
  constructor(private http: HttpClient) {}
  
  apps: any;
  ownedapps: any;
  
  
  getApps() {
    this.http.get(url_apps).subscribe(res => {
      this.apps = res;
    });
  }   
  
  getOwnedApps() {
    this.http.get(url_owned_apps).subscribe(res => {
      this.ownedapps = res;
    });
  }    

  addOwnedApps(payload) {
  this.http.post(url_owned_apps, payload).subscribe();
  this.getOwnedApps(); // refresh data
}
  
  
}

