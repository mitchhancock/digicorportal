import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AeroNewsComponent } from './aero-news.component';

describe('AeroNewsComponent', () => {
  let component: AeroNewsComponent;
  let fixture: ComponentFixture<AeroNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AeroNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AeroNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
