import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlatformNewsFakeDb } from '../../../../dataSources/platform-news';


@Component({
    selector   : 'app-platform-news',
    templateUrl: './platform-news.component.html',
    styleUrls  : ['./platform-news.component.scss']
})
export class PlatformNewsComponent implements OnInit
{
    news: any[];
    loadingIndicator = true;
    reorderable = true;

    constructor(private http: HttpClient)
    {

    }

    ngOnInit()
    {
        this.http.get('api/platform-news')
            .subscribe((news: any) => {
                this.news = news;
                this.loadingIndicator = false; 
            });
  }
              
}

