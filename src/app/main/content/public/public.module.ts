import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../core/modules/shared.module';

import {MatTableModule} from '@angular/material';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';

import { FusePublicComponent } from './public.component';
import { AeroNewsComponent } from './aero-news/aero-news.component';
import { PlatformNewsComponent } from './platform-news/platform-news.component';
import { PlatformKpiComponent } from './platform-kpi/platform-kpi.component';
import { NewsComponent } from './news/news.component';
import { AppStoreComponent } from './app-store/app-store.component';
import { AppstoreService } from './app-store/appstore.service';
import { MembersComponent } from './members/members.component';
import { MembersService } from './members/members.service';

const routes = [
    {
        path: 'public',
        component: FusePublicComponent
    }
];

@NgModule({
    declarations: [
        FusePublicComponent,
        AeroNewsComponent,
        NewsComponent,
        AppStoreComponent,
        PlatformNewsComponent,
        PlatformKpiComponent,
        MembersComponent
    ],
    imports: [
        SharedModule,
//        RouterModule.forChild(routes),
        MatTableModule,
        FuseWidgetModule
    ],
    exports: [
        FusePublicComponent
    ],
    providers: [
      AppstoreService,
      MembersService
    ]
})

export class FusePublicModule {
}
