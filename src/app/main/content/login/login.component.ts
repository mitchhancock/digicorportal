import { Component, OnInit, Injectable } from '@angular/core';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { FuseConfigService } from './../../../core/services/config.service';
import { fuseAnimations } from './../../../core/animations';
import { Router, RouterLink } from '@angular/router';
import { PortaluserService } from './../../portaluser.service'; 

@Component({
    selector   : 'portal-login',
    templateUrl: './login.component.html',
    styleUrls  : ['./login.component.scss'],
    animations : fuseAnimations
})

export class LoginComponent implements OnInit
{
    loginForm: FormGroup;
    loginFormErrors: any;
    loginResult: boolean;
    username: string;
    password: string;
    hide = true;
  
    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private userService: PortaluserService
        )
    {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });

        this.loginFormErrors = {
            username : {},
            password : {}
        };
      
      this.loginResult = true;
      this.username = '';
      this.password = '';
    }

  onLogin(username, password) {
    if (username === 'digicor' && password === 'project') {
      // -- login 
      this.loginResult = true;
      this.userService.loginUser();
      this.router.navigate(['/home']);
    } 
    else   {
      // -- wrong credentials
      this.loginResult = false;
      
    }
  }
  
  
    ngOnInit()
    {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    onLoginFormValuesChanged()
    {
        for ( const field in this.loginFormErrors )
        {
            if ( !this.loginFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }
}
