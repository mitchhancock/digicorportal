import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitchHomeComponent } from './mitch-home.component';

describe('MitchHomeComponent', () => {
  let component: MitchHomeComponent;
  let fixture: ComponentFixture<MitchHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitchHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitchHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
