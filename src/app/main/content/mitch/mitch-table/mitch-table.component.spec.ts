import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitchTableComponent } from './mitch-table.component';

describe('MitchTableComponent', () => {
  let component: MitchTableComponent;
  let fixture: ComponentFixture<MitchTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitchTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitchTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
