import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitchLoginComponent } from './mitch-login.component';

describe('MitchLoginComponent', () => {
  let component: MitchLoginComponent;
  let fixture: ComponentFixture<MitchLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitchLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitchLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
