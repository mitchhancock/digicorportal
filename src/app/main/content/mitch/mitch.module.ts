import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../core/modules/shared.module';
import { FuseMitchComponent } from './mitch.component';
import { MitchHomeComponent } from './mitch-home/mitch-home.component';
import { MitchLoginComponent } from './mitch-login/mitch-login.component';
import { MitchTableComponent } from './mitch-table/mitch-table.component';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';

const childRoutes = [
    {
        path: 'apply',   
        redirectTo: 'apply-member'
    }
];

const routes = [
    {
        path     : 'mitch', 
        component: FuseMitchComponent
    }
];

@NgModule({
    declarations: [
        FuseMitchComponent,
        MitchHomeComponent,
        MitchLoginComponent,
        MitchTableComponent
    ],
    imports     : [
        SharedModule,
//        RouterModule.forChild(routes),
        SharedModule,
        FuseWidgetModule,
        BrowserModule,
        RouterModule.forChild(childRoutes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCCbIepK2AE-GPYCocWW7WY2_HT9-MLM3g'
        }),
    ],
    exports     : [
        FuseMitchComponent
    ]
})

export class FuseMitchModule
{
}
