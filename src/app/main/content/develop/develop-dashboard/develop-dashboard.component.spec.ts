import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopDashboardComponent } from './develop-dashboard.component';

describe('DevelopDashboardComponent', () => {
  let component: DevelopDashboardComponent;
  let fixture: ComponentFixture<DevelopDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
