import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopRunningProjectsComponent } from './develop-running-projects.component';

describe('DevelopRunningProjectsComponent', () => {
  let component: DevelopRunningProjectsComponent;
  let fixture: ComponentFixture<DevelopRunningProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopRunningProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopRunningProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
