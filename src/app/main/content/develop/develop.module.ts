import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseDevelopComponent } from './develop.component';
import { DevelopDashboardComponent } from './develop-dashboard/develop-dashboard.component';
import { DevelopRunningProjectsComponent } from './develop-running-projects/develop-running-projects.component';

const routes = [
    {
        path     : 'develop', 
        component: FuseDevelopComponent
    }
];

@NgModule({
    declarations: [
        FuseDevelopComponent,
        DevelopDashboardComponent,
        DevelopRunningProjectsComponent
    ],
    imports     : [
        SharedModule,
//        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseDevelopComponent
    ]
})

export class FuseDevelopModule
{
}
