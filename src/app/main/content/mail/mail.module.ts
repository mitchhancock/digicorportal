import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { HttpClient } from '@angular/common/http';

import { FuseMailComponent } from './mail.component';
import { ReadMailComponent } from './read-mail/read-mail.component';
import { SendMessageComponent } from './send-message/send-message.component';

const routes = [
    {
        path     : 'apps/mail', 
        component: FuseMailComponent
    }
];

@NgModule({
    declarations: [
        FuseMailComponent,
        ReadMailComponent,
        SendMessageComponent
    ],
    imports     : [
      SharedModule,
      FuseWidgetModule
//        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseMailComponent
    ]
})

export class FuseMailModule
{
}
