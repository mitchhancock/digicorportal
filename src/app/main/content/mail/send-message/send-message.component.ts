import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { FuseNavigationService } from './../../../../core/components/navigation/navigation.service';
import { NavigationModelLogin } from './../../../../navigation-custom.model';
import { Router } from '@angular/router';
import { UserService } from './../../../user.service';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class SendMessageComponent implements OnInit {

  emailForm: FormGroup;
  emailFormErrors: any;
  emailResult: boolean;
  toAddress: string;
  subject: string;
  content: string;
  loggedUser: string = 'undefiniert';
  hide = true;
  naviModelLogin: NavigationModelLogin;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private navbarService: FuseNavigationService,
    private http: HttpClient
  ) {
    this.naviModelLogin = new NavigationModelLogin();

    this.emailFormErrors = {
      toAddress: {},
      subject: {},
      content: {}
    };

    this.emailResult = true;
    this.toAddress = '';
    this.subject = '';
    this.content = '';
    this.loggedUser = this.userService.actualUser.name;

  }

  getLoginStatus(): boolean {
    return this.userService.isLoggedIn();
  }

  getUser(): string {
    return this.userService.actualUser.name;
  }

  getAvatar(): string {
    return this.userService.actualUser.avatar;
  }

  logout(): void {
    // -- logout
    this.userService.logout();
    this.router.navigate(['/home']);
  }

  onSend(toAddress, subject, content) {
    let host = "chimail.uk2.net";
    let username = "mitchell.hancock@informationcatalyst.com";
    let password = "mitchell2016";
    let port = 587;
    this.http.get("http://icemain.hopto.org:8053/portalbackend/rest/postdata/sendMail?host=" + host + "&username=" + username + "&password=" + password + "&port=" + port + "&subject=" + subject + "&content=" + content + "&to="+toAddress).subscribe(data => {
      console.log(data);
    });
  }

  ngOnInit() {
    this.emailForm = this.formBuilder.group({
      toAddress: ['', Validators.email],
      subject: ['', Validators.required],
      content: ['']
    });

    this.emailForm.valueChanges.subscribe(() => {
      this.onemailFormValuesChanged();
    });
  }

  onemailFormValuesChanged() {
    for (const field in this.emailFormErrors) {
      if (!this.emailFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.emailFormErrors[field] = {};

      // Get the control
      const control = this.emailForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.emailFormErrors[field] = control.errors;
      }
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed

  }

}
