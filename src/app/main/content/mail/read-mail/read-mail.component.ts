import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-read-mail',
  templateUrl: './read-mail.component.html',
  styleUrls: ['./read-mail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class ReadMailComponent implements OnInit {

  mail: any[];
  owned_mail: any[];
  loadingIndicator = true;
  reorderable = true;
  selectedMail: any[];
  temp: any[];
  canBuy: boolean = false;

  searchText: string = '';


  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/getMail').subscribe(data => {
      console.log(data);
      this.mail = <any[]>data;
      this.loaded();
    });
  }

  deleteEmail(event) {
    this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/deleteMail&subject=' + this.selectedMail[0].subject + '&date=' + this.selectedMail[0].sentDate + '&from=' + this.selectedMail[0].from).subscribe(data => {
      console.log(data);
      this.mail = <any[]>data;
      this.loaded();
    });
  }

  loaded() {
  console.log(this.mail);
  this.searchText = '';
  this.loadingIndicator = false;
  this.temp = this.mail;
  this.owned_mail = this.temp;

  // select 1st row
  this.selectedMail = [this.mail[0]];
}

  onMailSelect({ selected }) {
    // update selected row
    this.selectedMail = selected;
    this.canBuy = (this.findMailByFrom(this.owned_mail, this.selectedMail[0].from) == null);
    //console.log(this.selectedMail);
  }

  addMailtoOwnedmail() {
    
    let newRow = { from: this.selectedMail[0].from, subject: this.selectedMail[0].subject, recipients: this.selectedMail[0].recipients, sentDate: this.selectedMail[0].sentDate, content: this.selectedMail[0].content};

    // reload data
    this.owned_mail = this.mail;
    this.canBuy = false;

  }

  updateFilter(event) {
    const val = this.searchText.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return (d.from.toLowerCase().indexOf(val) !== -1 ||
        d.subject.toLowerCase().indexOf(val) !== -1 ||
        d.sentDate.toLowerCase().indexOf(val) !== -1 ||
        d.content.toLowerCase().indexOf(val) !== -1 ||
        d.recipients.toLowerCase().indexOf(val) !== -1) || !val;
    });

    // update the rows
    this.mail = temp;

    // Select 1st
    if (this.mail.length > 0) {
      this.selectedMail = [this.mail[0]];
      this.canBuy = (this.findMailByFrom(this.owned_mail, this.selectedMail[0].from) == null);
    }
  }

  findMailByFrom(MailList: any[], from: number) {
    return MailList.find(x => x.from === from);
  }

  onMailActivate(event) {
    // console.log('Activate Event', event);
  }

}
