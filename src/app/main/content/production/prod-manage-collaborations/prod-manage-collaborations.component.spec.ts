import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdManageCollaborationsComponent } from './prod-manage-collaborations.component';

describe('ProdManageCollaborationsComponent', () => {
  let component: ProdManageCollaborationsComponent;
  let fixture: ComponentFixture<ProdManageCollaborationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdManageCollaborationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdManageCollaborationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
