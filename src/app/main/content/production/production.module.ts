import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseProductionComponent } from './production.component';
import { ProdDashboardComponent } from './prod-dashboard/prod-dashboard.component';
import { ProdOrdersComponent } from './prod-orders/prod-orders.component';
import { ProdManageCollaborationsComponent } from './prod-manage-collaborations/prod-manage-collaborations.component';

const routes = [
    {
        path     : 'production', 
        component: FuseProductionComponent
    }
];

@NgModule({
    declarations: [
        FuseProductionComponent,
        ProdDashboardComponent,
        ProdOrdersComponent,
        ProdManageCollaborationsComponent
    ],
    imports     : [
        SharedModule,
//        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseProductionComponent
    ]
})

export class FuseProductionModule
{
}
