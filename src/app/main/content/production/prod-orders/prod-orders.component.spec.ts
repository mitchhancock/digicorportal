import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdOrdersComponent } from './prod-orders.component';

describe('ProdOrdersComponent', () => {
  let component: ProdOrdersComponent;
  let fixture: ComponentFixture<ProdOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
