import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';

import { FuseAdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { UserService} from './../../user.service';

const routes = [
    {
        path     : 'admin', 
        component: FuseAdminComponent
    }
];

@NgModule({
    declarations: [
        FuseAdminComponent,
        AdminDashboardComponent,
        AdminUserComponent
    ],
    imports     : [
        SharedModule,
//        RouterModule.forChild(routes)
    ],
    providers: [
      UserService
    ],
    exports     : [
        FuseAdminComponent
    ]
})

export class FuseAdminModule
{
}
