import { Component, OnInit } from '@angular/core';
import { UserService } from './../../../user.service';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.scss']
})
export class AdminUserComponent implements OnInit {

  public users: any[];
  selectedUser: any[];
  loadingIndicator = true;
  reorderable = true;
  temp: any[];
  searchText: string = '';
  
  constructor(private userData: UserService) {}

  ngOnInit() {
  this.searchText = '';
   // get data from DB
   this.users = this.userData.users;
   this.loadingIndicator = false;
   
   this.temp = this.users;
   
   // select 1st row
   if (this.users != null) {this.selectedUser = [this.users[0]]; }
     
  }

  onUsersSelect({ selected }) {
    // update selected row
    this.selectedUser = selected;
  }
  
  updateFilter(event) {
    const val = this.searchText.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return (d.name.toLowerCase().indexOf(val) !== -1 ||
              d.lastName.toLowerCase().indexOf(val) !== -1 ||
              d.company.toLowerCase().indexOf(val) !== -1 ||
              d.email.toLowerCase().indexOf(val) !== -1) || !val;
     });
  
    // update the rows
    this.users = temp;
    
    // Select 1st
    if (this.users != null){
       this.selectedUser = [this.users[0]];  
       }
  }
  
  onUsersActivate(event) {
    // console.log('Activate Event', event);
  }
  
}
