import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../core/modules/shared.module';
import { ApplyMemberComponent } from './apply-member.component';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';

const childRoutes = [
  {
    path: 'apply',
    redirectTo: 'apply-member'
  }
];

const routes = [
  {
    path: 'apply',
    component: ApplyMemberComponent
  }
];

@NgModule({
  declarations: [
    ApplyMemberComponent
  ],
  imports: [
    SharedModule,
    //        RouterModule.forChild(routes),
    SharedModule,
    FuseWidgetModule,
    BrowserModule,
    RouterModule.forChild(childRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCCbIepK2AE-GPYCocWW7WY2_HT9-MLM3g'
    }),
  ],
  exports: [
    ApplyMemberComponent
  ]
})

export class ApplyMemberModule {
}
