import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router} from '@angular/router';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-apply-member',
  templateUrl: './apply-member.component.html',
  styleUrls: ['./apply-member.component.scss']
})
export class ApplyMemberComponent implements OnInit {

  registerForm: FormGroup;
  registerFormErrors: any;
  registerResult: boolean;
  firstName: string;
  lastName: string;
  company: string;
  username: string;
  password: string;
  email: string;
  avatar: string;
  loggedUser: string = 'undefiniert';
  hide = true;

  @ViewChild('fileInput') fileInput;


    constructor(
     private router: Router,
     private formBuilder: FormBuilder,
     private http: HttpClient
) {
  
     this.registerFormErrors = {
            username : {},
            password: {},
            firstName : {},
            lastName: {},
            company : {},
            email: {}
        };
      
      this.registerResult = true;
      this.username = '';
      this.password = '';
      this.firstName = '';
      this.lastName = '';
      this.company = '';
      this.email = '';
      this.avatar = ''; //TO DO
    
      
   }
  
    onRegister(username, password, firstName, lastName, company, email) {

      //Upload user icon to database
      let fileBrowser = this.fileInput.nativeElement;
      if (fileBrowser.files && fileBrowser.files[0]) {
        let file = fileBrowser.files[0];
        this.avatar = file.name;
        const formData = new FormData();
        formData.append("image", file);
        this.http.post('http://icemain.hopto.org:8053/portalbackend/upload.jsp', formData).subscribe(res => {
        });
      }

      //Send information to users database
      this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/register?username='
        + username + '&password=' + password + '&firstName=' + firstName + '&lastName=' + lastName
        + '&company=' + company + '&email=' + email + '&avatar=' + this.avatar)
        .subscribe(data => {

        });
      this.router.navigate(['/home']);
  }
  
 ngOnInit()
    {
        this.registerForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            company: ['', Validators.required],
            email: ['', Validators.required],
            avatar: ['']
        });

        this.registerForm.valueChanges.subscribe(() => {
            this.onRegisterFormValuesChanged();
        });
    }

    onRegisterFormValuesChanged()
    {
        for ( const field in this.registerFormErrors )
        {
            if ( !this.registerFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.registerFormErrors[field] = {};

            // Get the control
            const control = this.registerForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.registerFormErrors[field] = control.errors;
            }
        }
    }

   ngOnDestroy() {
        // prevent memory leak when component destroyed
         
    }

}
