import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRunningComponent } from './sales-running.component';

describe('SalesRunningComponent', () => {
  let component: SalesRunningComponent;
  let fixture: ComponentFixture<SalesRunningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRunningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRunningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
