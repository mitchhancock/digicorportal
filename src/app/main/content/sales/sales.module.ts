import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { HttpClient } from '@angular/common/http';

import { FuseSalesComponent } from './sales.component';
import { SalesDashboardComponent } from './sales-dashboard/sales-dashboard.component';
import { SalesNewCallComponent } from './sales-new-call/sales-new-call.component';
import { SalesMyTendersComponent } from './sales-my-tenders/sales-my-tenders.component';
import { SalesRunningComponent } from './sales-running/sales-running.component';


const routes = [
    {
        path     : 'sales', 
        component: FuseSalesComponent
    }
];

@NgModule({
    declarations: [
        FuseSalesComponent,
        SalesDashboardComponent,
        SalesNewCallComponent,
        SalesMyTendersComponent,
        SalesRunningComponent
    ],
    imports     : [
      SharedModule,
      FuseWidgetModule
//        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseSalesComponent
    ]
})

export class FuseSalesModule
{
}
