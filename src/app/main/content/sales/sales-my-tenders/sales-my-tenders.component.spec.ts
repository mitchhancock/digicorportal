import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesMyTendersComponent } from './sales-my-tenders.component';

describe('SalesMyTendersComponent', () => {
  let component: SalesMyTendersComponent;
  let fixture: ComponentFixture<SalesMyTendersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesMyTendersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesMyTendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
