import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AppStoreFakeDb } from '../../../../dataSources/app-store';
import { AppstoreService } from '../../public/app-store/appstore.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-sales-my-tenders',
  templateUrl: './sales-my-tenders.component.html',
  styleUrls: ['./sales-my-tenders.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class SalesMyTendersComponent implements OnInit {

  tenders: any[];
  owned_tenders: any[];
  loadingIndicator = true;
  reorderable = true;
  selectedTender: any[];
  temp: any[];
  canBuy: boolean = false;

  searchText: string = '';


  constructor(private dataService: AppstoreService, private http: HttpClient) { }

  ngOnInit() {

    this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/getAllTenders').subscribe(data => {
      console.log(data);
      this.tenders = <any[]>data;
      this.loaded();
    });
  }

  loaded() {
  console.log(this.tenders);
  this.searchText = '';
  // get data from DB
  //this.dataService.getTenders();
  //this.dataService.getOwnedTenders();
  this.loadingIndicator = false;

  // init datasets
  //this.tenders = this.dataService.tenders;
  this.temp = this.tenders;
  this.owned_tenders = this.temp;
  // select 1st row
  this.selectedTender = [this.tenders[0]];
}

  onTendersSelect({ selected }) {
    // update selected row
    this.selectedTender = selected;
    this.canBuy = (this.findTenderById(this.owned_tenders, this.selectedTender[0].id) == null);
    console.log(this.selectedTender);
  }

  addTendertoOwnedTenders() {
    // push selected dataset to OwnedTenders

    let newRow = { id: this.selectedTender[0].id, title: this.selectedTender[0].title, type: this.selectedTender[0].type, submissionDeadline: this.selectedTender[0].submissionDeadline, status: this.selectedTender[0].status };

    // reload data
    this.owned_tenders = this.tenders;
    this.canBuy = false;

  }

  updateFilter(event) {
    const val = this.searchText.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return (d.title.toLowerCase().indexOf(val) !== -1 ||
        d.type.toLowerCase().indexOf(val) !== -1 ||
        d.submissionDeadline.toLowerCase().indexOf(val) !== -1 ||
        d.status.toLowerCase().indexOf(val) !== -1) || !val;
    });

    // update the rows
    this.tenders = temp;

    // Select 1st
    if (this.tenders.length > 0) {
      this.selectedTender = [this.tenders[0]];
      this.canBuy = (this.findTenderById(this.owned_tenders, this.selectedTender[0].id) == null);
    }
  }

  findTenderById(tenderList: any[], id: number) {
    return tenderList.find(x => x.id === id);
  }

  onTendersActivate(event) {
    // console.log('Activate Event', event);
  }

}
