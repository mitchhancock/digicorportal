import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'app-sales-new-call',
  styleUrls: ['sales-new-call.component.scss'],
  templateUrl: 'sales-new-call.component.html'
})

export class SalesNewCallComponent {
  displayedColumns = ['id', 'title', 'customer', 'issueDate', 'submissionDeadline', 'owner', 'description'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient) { }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit(): void {
    this.http.get('http://icemain.hopto.org:8053/portalbackend/rest/postdata/getAllTenders').subscribe(data => {
      const users: UserData[] = [];
      let dataArr = <Array<UserData>>data;
      for (let entry of dataArr) {
        users.push(<UserData>entry);
      }

      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

}

export interface UserData {
  id: string;
  title: string;
  customer: string;
  issueDate: string;
  submissionDeadline: string;
  owner: string;
  description: string;
}


