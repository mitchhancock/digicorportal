import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesNewCallComponent } from './sales-new-call.component';

describe('SalesNewCallComponent', () => {
  let component: SalesNewCallComponent;
  let fixture: ComponentFixture<SalesNewCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesNewCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesNewCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
