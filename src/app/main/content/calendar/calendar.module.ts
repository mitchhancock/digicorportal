import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../core/components/widget/widget.module';
import { HttpClient } from '@angular/common/http';
import { FuseCalendarComponent } from './calendar.component';


const routes = [
    {
        path     : 'apps/calendar', 
        component: FuseCalendarComponent
    }
];

@NgModule({
    declarations: [
        FuseCalendarComponent
    ],
    imports     : [
      SharedModule,
      FuseWidgetModule

//        RouterModule.forChild(routes)
    ],
    exports     : [
        FuseCalendarComponent
    ]
})

export class FuseCalendarModule
{
}
