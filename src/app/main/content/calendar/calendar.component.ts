import { Component, OnInit } from '@angular/core';
import { UserService } from './../../user.service';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
    selector   : 'fuse-calendar',
    templateUrl: './calendar.component.html',
    styleUrls  : ['./calendar.component.scss']
})
export class FuseCalendarComponent implements OnInit
{
  private srcUrl: SafeResourceUrl;
  private calUser: string;

  constructor(private userService: UserService, public sanitizer: DomSanitizer) 
  {
    //this.calUser = this.getCalendarUser();
    //this.srcUrl =  "https://calendar.google.com/calendar/embed?src="+this.calUser+"&ctz=Europe%2FLondon";
    
  }

    ngOnInit() {
      this.calUser = this.getCalendarUser();
      this.srcUrl = "https://calendar.google.com/calendar/embed?src=" + this.calUser + "&ctz=Europe%2FLondon";
    }

  getCalendarUser(): string {
    return this.userService.actualUser.calendarUsername;
  }

  //getSrcUrl() {
  //  console.log(this.srcUrl);
  //  return this.sanitizer.bypassSecurityTrustResourceUrl(this.srcUrl);
  //}

}
