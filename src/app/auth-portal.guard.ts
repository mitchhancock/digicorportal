import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PortaluserService } from './main/portaluser.service';

@Injectable()
export class AuthPortalGuard implements CanActivate {
  
constructor(private portaluserService: PortaluserService) {
  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      // console.log('PortalUser-Guard: ' + this.portaluserService.isLoggedIn());
       return true; // development modus
      //  return this.portaluserService.isLoggedIn();
  }
}
