export class NavigationModelLogout {
  public model: any[];
  
  constructor() {
    
    this.model = [{
        'id': 'home',
        'title': 'Welcome',
        'type': 'item',
        'icon': 'item',
        'url': '/home'

      }];
  }
}


export class NavigationModelLogin {
  public model: any[];
  
  constructor() {
    
    this.model = [{
        'id': 'home',
        'title': 'Welcome',
        'type': 'item',
        'icon': 'item',
        'url': '/home'

      },
      {
        'id': 'dashboards',
        'title': 'Dashboards',
        'type': 'group',
        'children': [

          {
            'id': 'public',
            'title': 'Public area',
            'type': 'item',
            'icon': 'item',
            'url': '/public'

          },
          {
            'id': 'sales',
            'title': 'Sales',
            'type': 'item',
            'icon': 'item',
            'url': '/sales'

          },
          {
            'id': 'mail',
            'title': 'Mail',
            'type': 'item',
            'icon': 'item',
            'url': '/apps/mail'

          },
          {
            'id': 'calendar',
            'title': 'Calendar',
            'type': 'item',
            'icon': 'item',
            'url': '/apps/calendar'

          },
          {
            'id': 'mitch',
            'title': 'Mitch',
            'type': 'item',
            'icon': 'item',
            'url': '/mitch'

          },
          {
            'id': 'develop',
            'title': 'Development',
            'type': 'item',
            'icon': 'item',
            'url': '/develop'

          },
          {
            'id': 'production',
            'title': 'Production',
            'type': 'item',
            'icon': 'item',
            'url': '/production'

          },
          {
            'id': 'admin',
            'title': 'Administration',
            'type': 'item',
            'icon': 'item',
            'url': '/admin'

          }
        ]
      }
    ];
  }
}
