export class MembersFakeDb
{
    public static members = [
        {
            'id'             :  1,
            'companyName'    : 'E.I.S. Electronics',
            'city'           : 'Bremenhaven',
            'country'        : 'Germany',
            'type'           : 'Repair',
            'activities'     : 'Cabin Systems, Maintenance, Avionics, Electricals, Elektromechanics, Electronics, Interiors, Lavatories',
            'region'         : 'Europe, Germany, Africa, Asia, Australia, Middle East, North America, South America',
            'certificates'   : 'EASA-Part21G , EASA PART 145 , EN 9100 , AS 9100, AQAP, ISO 9001 /AS9100'
        },
          {
            'id'             :  2,
            'companyName'    : 'Seattle Turbine Parts',
            'city'           : 'Puyallup',
            'country'        : 'United States',
            'type'           : 'OEM, Overhail, Repair',
            'activities'     : 'CabCommunication, Overhaul, Repair',
            'region'         : 'Europe, Middle East, North America, South America',
            'certificates'   : 'EASA-Part21G , EASA PART 145 , EN 9100 , AS 9100, AQAP, ISO 9001 /AS9100'
        },
         {
            'id'             : 3,
            'companyName'    : 'INNOVINT Aircraft Interior GmbH',
            'city'           : 'Hamburg',
            'country'        : 'Germany',
            'type'           : 'Repair',
            'activities'     : 'Sales, Production, Engineering, Design',
            'region'         : 'EurMiddle East, Europe, Germany, North America, Africa, Asia, South America',
            'certificates'   : 'EASA PART 145 , EN 9100 , EASA-Part21G'
        },
    
    ];
  
}
