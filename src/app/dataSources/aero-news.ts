export class AeroNewsFakeDb
{
    public static aeronews = [
        {
            'headline' : 'Tendions ease over Pratt engine supply to Airbus-sources',
            'newsbody': 'Oct 23, 2017 - Airbus (AIR.PA) is more confident in the ability of Pratt & Whitney (UTX.N) to speed up delayed engine shipments, two people familiar with the ...', 
            'logo'     : 'assets/images/news/airbus.png',
        },
        {
            'headline' : 'Industry analyst unclear about long term future of Bombardier commercial aircrafts',
            'newsbody': 'Oct 23, 2017 - Bombardier remains burdened by more than US$9 billion of debt and wants to regain its leading position in the business jet market.', 
            'logo'     : 'assets/images/news/bombadier.png',
        },
        {
            'headline' : 'Tendions ease over Pratt engine supply to Airbus-sources',
            'newsbody': 'Oct 23, 2017 - Airbus (AIR.PA) is more confident in the ability of Pratt & Whitney (UTX.N) to speed up delayed engine shipments, two people familiar with the ...', 
            'logo'     : '/assets/images/news/pratt_whitney.png',
        }];
}
