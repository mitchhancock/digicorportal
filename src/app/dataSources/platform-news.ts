export class PlatformNewsFakeDb
{
    public static news = [
        {
            'headline' : 'New member joint hte DIGICOR platform',
            'newsbody': 'Oct 23, 2017 - AeroSpace Ltd. join the platform and will offer services for the logistics and maintenance for Airbus & Boing aircrafts...', 
            'logo'     : 'assets/images/news/aero_ltd.jpg',
        },
        {
            'headline' : 'New risk assessment function available',
            'newsbody': 'Oct 21, 2017 - ALM has renewed the @RISK App for risk assessment of collaboration partnerships...', 
            'logo'     : 'assets/images/news/risk_app.jpg',
        }
      ];
}
