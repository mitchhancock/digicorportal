export class AppStoreFakeDb
{
    public static apps = [
        {
            'id'       : 1,
            'name'     : 'Scheduler',
            'provider' : 'Certicon AS',
            'type'     : 'Planning/Scheduling',
            'pay_model': 'license fee, monthly subscription, pay per use'
        },
       { 
            'id'       : 2,
            'name'     : 'atRisk',
            'provider' : 'Almende',
            'type'     : 'Risk management',
            'pay_model': 'monthly subscription, pay per use'
        },
       { 
            'id'       : 3,
            'name'     : 'atMatch',
            'provider' : 'Uni Manchester',
            'type'     : 'Collaboration',
            'pay_model': 'license fee'
        },
        { 
            'id'       : 4,
            'name'     : 'AiB-plan',
            'provider' : 'ADS',
            'type'     : 'Planning/ Scheduling',
            'pay_model': 'monthly subscription'
        },
        { 
            'id'       : 5,
            'name'     : 'BestProj',
            'provider' : 'Company XYZ',
            'type'     : 'Project management',
            'pay_model': 'monthly subscription'
        },
        { 
            'id'       : 6,
            'name'     : 'SuperCosts',
            'provider' : 'Company XYZ',
            'type'     : 'Costing',
            'pay_model': 'monthly subscription'
        },
        { 
            'id'       : 7,
            'name'     : 'FindSupplier',
            'provider' : 'Company XYZ',
            'type'     : 'Collaboration',
            'pay_model': 'monthly subscription'
        },
        { 
            'id'       : 8,
            'name'     : 'atLogistics',
            'provider' : 'Cocoon',
            'type'     : 'Logistics',
            'pay_model': 'pay per use'
        }
      ];
  
    public static owned_apps = [
        {
            'id'       : 1,
            'name'     : 'Scheduler',
            'provider' : 'Certicon AS',
            'type'     : 'Planning/ Scheduling',
            'status'   : 'available'
        },
       { 
            'id'       : 2,
            'name'     : 'atRisk',
            'provider' : 'Almende',
            'type'     : 'Risk management',
            'status'   : 'install...'
        },
       { 
            'id'       : 3,
            'name'     : 'atMatch',
            'provider' : 'Uni Manchester',
            'type'     : 'Collaboration',
            'status'   : 'available'
        },
        { 
            'id'       : 4,
            'name'     : 'AiB-plan',
            'provider' : 'ADS',
            'type'     : 'Planning/ Scheduling',
            'status'   : 'available'
        },
        { 
            'id'       : 8,
            'name'     : 'atLogistics',
            'provider' : 'Cocoon',
            'type'     : 'Logistics',
            'status'   : 'running service'
        }];
  
  
  
}
