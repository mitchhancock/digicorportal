export class UserFakeDb
{
    public static user = [
              {
            'id'      : 0,
            'name'    : 'Guest',
            'lastName': 'Guest',
            'company' : '',
            'password': 'guest',
            'username': 'guest',
            'email'   : '',
            'avatar'  : 'assets/images/avatars/profile.jpg'            
        },
        {
            'id'      : 1,
            'name'    : 'Arnd',
            'lastName': 'Schirrmann',
            'company' : 'Airbus',
            'password': 'arnd',
            'username': 'arnd',
            'email'   : 'arnd.schirrmann@airbus.com',   
            'avatar' : 'assets/images/avatars/arnd.jpg'            
        },
        {
            'id'      : 2,
            'name'    : 'Gash',
            'lastName': 'Bullar',
            'company' : 'Control 2K',
            'username': 'gash',
            'password': 'gash',
            'email'   : 'gbhullar@control2k.co.uk',
            'avatar' : 'assets/images/avatars/profile.jpg'
 
        },
        {
            'id'      : 3,
            'name'    : 'Usman',
            'lastName': 'Wajid',
            'company' : 'ICE',
            'username': 'usman',
            'password': 'usman',
            'email'   : 'usman.wajid@informationcatalyst.com',
            'avatar' : 'assets/images/avatars/profile.jpg'
 
        },
        {
            'id'      : 4,
            'name'    : 'Ingo',
            'lastName': 'Martens',
            'company' : 'Hanse Aerospace',
            'username': 'ingo',
            'password': 'ingo',
            'email'   : 'i.martens@hanse-aerospace.net',
            'avatar' : 'assets/images/avatars/profile.jpg'
 
        }];
}
