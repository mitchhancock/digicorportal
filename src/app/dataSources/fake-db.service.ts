import { InMemoryDbService } from 'angular-in-memory-web-api';

import { UserFakeDb } from './user';
import { MembersFakeDb } from './members';
import { AeroNewsFakeDb } from './aero-news';
import { PlatformNewsFakeDb } from './platform-news';
import { AppStoreFakeDb } from './app-store';


export class FakeDbService implements InMemoryDbService
{
    createDb()
    {
        return {
            'members'           : MembersFakeDb.members,
            'aeronews'          : AeroNewsFakeDb.aeronews,
            'platform-news'     : PlatformNewsFakeDb.news,
            'apps'              : AppStoreFakeDb.apps,
            'owned-apps'        : AppStoreFakeDb.owned_apps,
            'user'              : UserFakeDb.user        
        };
    }
}
