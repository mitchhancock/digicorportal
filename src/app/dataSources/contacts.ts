export class ContactsFakeDb
{
    public static contacts = [
        {
            'name'    : 'Abbott',
            'lastName': 'Keitch',
            'company' : 'Saois',
        },
        {
            'name'    : 'Arnold',
            'lastName': 'Matlock',
            'company' : 'Laotcone',
        },
        {
            'name'    : 'Barrera',
            'lastName': 'Bradbury',
            'company' : 'Unizim',
        }];
}
